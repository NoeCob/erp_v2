import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { AuthorizationService } from '../../service/authorization.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsersService } from '../../service/users.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [AuthorizationService]
})
export class LoginComponent {
  loginForm: FormGroup;
  email: string;
  password: string;
  errors = [];
  loading = false;

  constructor(private router: Router,
    private formBuilder: FormBuilder,
    private authorizationService: AuthorizationService,
    private userService: UsersService,
  ) {

    this.loginForm = this.formBuilder.group({
      email   : ['', Validators.required],
      password: ['', Validators.required]
    });

    const access_token = window.localStorage.getItem('X-CSRF-TOKEN');
    // const access_token = window.localStorage.getItem('access_token');
    const user = window.localStorage.getItem('user');

/*     if (access_token != null && user != null) {
      console.log('hola');
      this.loading = true;
      const userData = JSON.parse(user);
      this.userService.user(userData.id).subscribe((data) => {
        if (data.errorMessage !== null) {
          localStorage.removeItem('access_token');
          localStorage.removeItem('user');
          this.errors.push('Su sesión Finalizo');
          this.loading = false;
        } else {
          this.router.navigate(['admin/inicio']);
        }
      });
    } */
  }

     logIn() {
      this.errors = [];
      if (this.loginForm.valid) {
        this.loading = true;
        this.authorizationService.login(this.loginForm.value).subscribe((data: any) => {
          console.log(data.code);
          if (data.code === 200) {
            console.log('Exito');
            // window.localStorage.setItem('access_token', data.response.auth_token.token);
            //window.localStorage.setItem('X-CSRF-TOKEN', data.response.auth_token.token);
            //window.localStorage.setItem('user', data.response.auth_token.user);
            // this.toastr.success('!Has ingresado exitosamente!');
            this.router.navigate(['/admin/inicio']);
          } else if (data.code === 0) {
            this.loading = false;
            this.errors = ['Error en el servidor'];
          } else {
            this.loading = false;
            //this.toastr.error('!Credenciales invalidas!');
            this.errors = data.errorMessage.error.user_authentication;
          }
        });
      }
    } 

/*   logIn() {
    this.errors = [];
    if (this.loginForm.valid) {
      this.loading = true;
      this.router.navigate['/admin/inicio'];
    }
  } */
}
