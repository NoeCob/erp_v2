import {Routes} from '@angular/router';
import {AuthGuardService} from '../../service/auth-guard.service';
import {ClientsComponent} from './clients.component';

export const ClientsRoutes: Routes = [
    {
        path: '',
        component: ClientsComponent
    },
    {
        path: '',
        children: [
            {path: 'clientes', component: ClientsComponent}
        ]
    }
]