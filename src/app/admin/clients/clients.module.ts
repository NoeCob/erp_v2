import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {ClientsRoutes} from './clients.routing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ClientsComponent } from './clients.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ClientsRoutes),
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        ClientsComponent
    ],
    entryComponents: [],
    exports: [ClientsComponent],
    providers:[]
})

export class ClientsModule {}