import {Routes} from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ClientsComponent } from './clients/clients.component';
// import {AuthGuardService} from '../service/auth-guard.service';

export const AdminRoutes: Routes = [
    {path: '', redirectTo: '/admin/inicio', pathMatch: 'full'},
    {path: 'inicio', redirectTo: '/admin/inicio', pathMatch: 'full'},
    {
        path: '',
        children: [
            { path: 'inicio', component: DashboardComponent },
            { path: '', 
              component: ClientsComponent,
              children:[{
                  path: 'clientes',
                  loadChildren: './clients/clients.module#ClientsModule'
              }]
            }
        ]
    }
];