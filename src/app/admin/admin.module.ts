import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutes } from './admin.routing';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ChartsModule } from 'ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown/public_api';
import { ButtonsModule } from 'ngx-bootstrap/buttons/public_api';
import { ClientsModule } from './clients/clients.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ChartsModule,
    // BsDropdownModule,
    //ButtonsModule.forRoot(),
    ReactiveFormsModule,
    RouterModule.forChild(AdminRoutes),
    ClientsModule
  ],
  declarations: [
    DashboardComponent
  ],
  entryComponents: [
  ]
})

export class AdminModule { }