import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {ReplaySubject} from 'rxjs/';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
    private apiService: ApiService
  ) {
  }

  user(val) {
    const response = new ReplaySubject<any>(1);
    this.apiService.get('administrator/users/' + val).subscribe(data => {
      response.next(data);
    });
    return response;
  }

  User() {
    const response = new ReplaySubject<any>(1);
    this.apiService.get('administrator/users/').subscribe(data => {
      response.next(data);
    });

    return response;
  }

  create(credentials) {
    const response = new ReplaySubject<any>(1);
    this.apiService.post('administrator/users/', credentials).subscribe(data => {
      response.next(data);
    });

    return response;
  }

  destroy(id) {
    const response = new ReplaySubject<any>(1);
    this.apiService.destroy('administrator/users/' + id).subscribe(data => {
      response.next(data);
    });
    return response;
  }

  show(id) {
    const response = new ReplaySubject<any>(1);
    this.apiService.get('administrator/users/' + id).subscribe(data => {
      response.next(data);
    });
    return response;
  }

/*   update(id, data) {
    const response = new ReplaySubject<any>(1);
    this.apiService.put('administrator/users/' + id, data).subscribe(dataResponse => {
      response.next(dataResponse);
    });
    return response;
  } */
}
