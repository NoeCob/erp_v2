import { throwError as observableThrowError, ReplaySubject } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})

export class ApiService {
    private headers = [];

    constructor(private http: HttpClient) {
    }

    getHeaders(): ReplaySubject<Object> {
        const res = new ReplaySubject(1);
        if (window.localStorage.getItem('X-CSRF-TOKEN')) {
            this.headers['Authorization'] = window.localStorage.getItem('X-CSRF-TOKEN');
        } else {
            this.headers['Authorization'] = '';
        }
        /* if (window.localStorage.getItem('access_token')) {
            this.headers['Authorization'] = 'Bearer ' + window.localStorage.getItem('access_token');
        } else {
            this.headers['Authorization'] = '';
        } */
        res.next(this.headers);
        return res;
    }

    post(url: string, body: Object, isBlob?: boolean): ReplaySubject<Object> {
        const response = new ReplaySubject<any>(1);
        this.getHeaders().subscribe(h => {
          const headers = new HttpHeaders({ 'Authorization': h['Authorization'] });
          let resFromApi = null;
          if (isBlob) {
            resFromApi = this.http.post(environment.apiUrl + url, body, {
              headers: headers,
              responseType: 'blob'
            });
          } else {
            resFromApi = this.http.post(environment.apiUrl + url, body, {
              headers: headers
            });
          }
          console.log(resFromApi);
          resFromApi.pipe(map((resp: any) => {
            return {
              code: 200,
              response: resp
            };
          }))
/*           .catch((error: any) => {
              console.log('no hola');
            return observableThrowError({
              code: error.status,
              errorMessage: error.error,
              detail: error.detail
            });
          }) */
            .subscribe(
              (res) => {
                response.next(res);
              },
              (err) => {
                response.next(err);
              });
        });
        return response;
      }
    // post(url: string, body: Object, isBlob?: boolean): ReplaySubject<Object> {
    //     const response = new ReplaySubject<any>(1);
    //     this.getHeaders().subscribe(h => {
    //         const headers = new HttpHeaders({ 'Authorization': h['Authorization'] });
    //         let resFromApi = null;
    //         if (isBlob) {
    //             resFromApi = this.http.post(environment.apiUrl + url, body, {
    //                 headers: headers,
    //                 responseType: 'blob'
    //             });
    //         } else {
    //             resFromApi = this.http.post(environment.apiUrl + url, body, {
    //                 headers: headers
    //             });
    //         }

    //         resFromApi.pipe(map((resp: any) => {
    //             return {
    //                 code: 200,
    //                 response: resp
    //             };
    //         }))
    //       /*   .catch((error: any) => {
    //             return observableThrowError({
    //                 code: error.status,
    //                 errorMessage: error.error,
    //                 detail: error.detail
    //             });
    //         }) */
    //             .subscribe(
    //                 (res) => {
    //                     response.next(res);
    //                 },
    //                 (err) => {
    //                     response.next(err);
    //                 });
    //     });
    //     return response;
    // }

    get(url: string): ReplaySubject<Object> {
        const response = new ReplaySubject<any>(1);
        this.getHeaders().subscribe(h => {
            const headers = new HttpHeaders({ 'Authorization': h['Authorization'] });
            let resFromApi = null;
            resFromApi = this.http.get(environment.apiUrl + url, {
                headers: headers
            });
            resFromApi.map((resp: any) => {
                return {
                    code: 200,
                    response: resp
                };
            }).catch((error: any) => {
                return observableThrowError({
                    code: error.status,
                    errorMessage: error.error,
                    detail: error.detail
                });
            })
                .subscribe(
                    (res) => {
                        response.next(res);
                    },
                    (err) => {
                        response.next(err);
                    });
        });
        console.log(response);
        return response;
    }

    put(url: string, body: Object): ReplaySubject<Object> {
        const response = new ReplaySubject<any>(1);
        this.getHeaders().subscribe(h => {
            const headers = new HttpHeaders({ 'Authorization': h['Authorization'] });
            let resFromApi = null;
            resFromApi = this.http.put(environment.apiUrl + url, body, {
                headers: headers
            });
            resFromApi.map((resp: any) => {
                return {
                    code: 200,
                    response: resp
                };
            }).catch((error: any) => {
                return observableThrowError({
                    code: error.status,
                    errorMessage: error.error,
                    detail: error.detail
                });
            })
                .subscribe(
                    (res) => {
                        response.next(res);
                    },
                    (err) => {
                        response.next(err);
                    });
        });

        return response;
    }

    destroy(url: string): ReplaySubject<Object> {
        const response = new ReplaySubject<any>(1);
        this.getHeaders().subscribe(h => {
            const headers = new HttpHeaders({ 'Authorization': h['Authorization'] });
            let resFromApi = null;
            resFromApi = this.http.post(environment.apiUrl + url, {
                headers: headers
            });
            resFromApi.map((resp: any) => {
                return {
                    code: 200,
                    response: resp
                };
            }).catch((error: any) => {
                return observableThrowError({
                    code: error.status,
                    errorMessage: error.error,
                    detail: error.detail
                });
            })
                .subscribe(
                    (res) => {
                        response.next(res);
                    },
                    (err) => {
                        response.next(err);
                    });
        });

        return response;
    }
}
