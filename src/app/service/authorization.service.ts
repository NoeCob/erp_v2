import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { ReplaySubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment'
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  constructor(
    private apiService: ApiService,
    private http: HttpClient
  ) { }

  getToken() {
    const response = new ReplaySubject<any>(1);
    this.http.get(environment.apiUrl + '/auth/token').subscribe(data => {
      localStorage.setItem('X-CSRF-TOKEN', JSON.stringify(data['token']));
      response.next(data);  
         
    });
    return response;
  }

  login(credentials) {
    const response = new ReplaySubject<any>(1);
    /* this.apiService.post('authenticate', credentials).subscribe(userData => {
      response.next(userData);
    }); */
  /*    this.http.post(environment.apiUrl + '/auth/login', credentials).subscribe(userData => {
       console.log(userData);
      response.next(userData);
    });  */
     this.apiService.post('/auth/login', credentials).subscribe(userData => {
       console.log(userData);
      response.next(userData);
    });

    return response;
  }

  isAuthenticated() {
    let isAuthorize = false;
    if (window.localStorage.getItem('access_token')) {
      isAuthorize = true;
    }
    return isAuthorize;
  }

}