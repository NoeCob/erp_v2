interface NavAttributes {
  [propName: string]: any;
}
interface NavWrapper {
  attributes: NavAttributes;
  element: string;
}
interface NavBadge {
  text: string;
  variant: string;
}
interface NavLabel {
  class?: string;
  variant: string;
}

export interface NavData {
  name?: string;
  url?: string;
  icon?: string;
  badge?: NavBadge;
  title?: boolean;
  children?: NavData[];
  variant?: string;
  attributes?: NavAttributes;
  divider?: boolean;
  class?: string;
  label?: NavLabel;
  wrapper?: NavWrapper;
}

export const navItems: NavData[] = [
  {
    name: 'Dashboard',
    url: '/admin/inicio',
    icon: 'fa fa-th-large'
  },
  {
    name: 'Clientes',
    url: '/admin/clientes',
    icon: 'fa fa-users'
  },
  {
    name: 'Servicio',
    url: '/servicio',
    icon: 'fa fa-ticket',
    class: 'servicio',
    children: [
      { name: 'Seguimiento', url: '/servicio/seguimiento' },
      { name: 'Mapa del seguimiento', url: '/servicio/mapa_del_seguimiento' },
      { name: 'Códigos de error', url: '/servicio/codigos_de_error' }
    ]
  },
  {
    name: 'Social',
    url: '/social',
    icon: 'fa fa-twitter',
    children: [
      { name: 'Galeria', url: '/social/galeria' },
      { name: 'Notificationes', url: '/social/notificaciones' }
    ]
  },
  {
    name: 'Finanzas',
    url: '/finanzas',
    icon: 'fa fa-usd',
    children: [
      { name: 'Clientes', url: '/finanzas/clientes' },
      { name: 'Responsabilidad Social', url: 'finanzas/clientes' },
      { name: 'Facturas', url: 'finanzas/facturas' },
      { name: 'Malas deudas', url: 'finanzas/malas_deudas' },
    ]
  },
  {
    name: 'Vendedores',
    url: '/vendedores',
    icon: 'fa fa-files-o',
    children: [
      { name: 'Contratos', url: '/vendedores/contratos' },
      { name: 'Extras', url: '/vendedores/extras' },
      { name: 'Grupos', url: 'vendedores/grupos' },
      { name: 'Comisiones', url: 'vendedores/comisiones' },
    ]
  },
  {
    name: 'Administración',
    url: '/administracion',
    icon: 'fa fa-user',
    children: [
      { name: 'Administradores', url: '/administracion/admin' },
      { name: 'Técnicos', url: '/administracion/tecnicos' }
    ]
  },
  {
    name: 'Dev Only',
    url: '/dev',
    icon: 'fa fa-bug',
    children: [
      { name: 'Asignación', url: '/dev/asignacion' },
      { name: 'Encuestas', url: '/dev/encuestas' }
    ]
  }
];
